# Auto Grouping Tools changelog

## 1.3.1 / 2023-07-26
List into chunks tool function added

## 1.3.0 / 2023-02-11
Callback posibility added for `auto_group_list_by_pkeys`, `auto_group_list` and `auto_group_list_multi` functions.

## 1.2.5 / 2023-02-11
`list_into_dict` added into `tools`.

## 1.2.4 / 2023-02-11
`dict_swap` and `map_dict_into_list` added into `tools`.

## 1.2.3 / 2023-01-26
`dict_filter` renamed to `dict_pass`. `dict_filter` added.

## 1.2.2 / 2023-01-12
Docs and example of auto_group_list_multi added.

## 1.2.1 / 2023-01-12
Exclude test files from package.

## 1.2.0 / 2023-01-12
New `advanced` functions added:
 - auto_group_list_into_tree_dict
 - auto_group_list_into_tree_list

New `tools` functions added:
 - sort_list_of_dicts
 - dict_filter

## 1.1.0 / 2023-01-12
New function `auto_group_list_multi` added.

## 1.0.0 / 2023-01-11
PIP3 comming out
