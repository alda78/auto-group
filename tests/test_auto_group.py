from auto_group import auto_group_dict
from auto_group import auto_group_list
from auto_group import auto_group_list_multi
from auto_group import auto_group_list_by_pkeys
from auto_group.tools import dict_filter


# #### test 1
_input1 = [
    {"a": 1, "b": 2, "c__a": 33, "c__b": 44},
    {"a": 1, "b": 2, "c__a": 55, "c__b": 66},
    {"a": 2, "b": 2, "c__a": 7, "c__b": 88},
    {"a": 2, "b": 2, "c__a": 77, "c__b": 99},
]
_expected1_1 = {
    "1": {"a": 1, "b": 2, "c": [{"a": 33, "b": 44}, {"a": 55, "b": 66}]},
    "2": {"a": 2, "b": 2, "c": [{"a": 7, "b": 88}, {"a": 77, "b": 99}]},
}
_expected1_1cb = {
    "1": {"a": 1, "c": [{"a": 33, "b": 44}, {"a": 55, "b": 66}]},
    "2": {"a": 2, "c": [{"a": 7, "b": 88}, {"a": 77, "b": 99}]},
}

_expected1_2 = {
    "1-2": {"a": 1, "b": 2, "c": [{"a": 33, "b": 44}, {"a": 55, "b": 66}]},
    "2-2": {"a": 2, "b": 2, "c": [{"a": 7, "b": 88}, {"a": 77, "b": 99}]},
}


def test_auto_group_list_by_pkeys():
    assert auto_group_list_by_pkeys(("a", ), _input1) == _expected1_1
    assert auto_group_list_by_pkeys(("a", "b"), _input1) == _expected1_2


def test_auto_group_list_by_pkeys_callback():
    assert auto_group_list_by_pkeys(("a",), _input1, each_result_item_callback=lambda item: item.pop("b")) == _expected1_1cb


def test_auto_group_list_by_pkeys_callback2():
    assert auto_group_list_by_pkeys(("a",), _input1, each_result_item_callback=lambda item: dict_filter(item, ["b"])) == _expected1_1cb


# #### test 2
_input2 = [
    {"a": 1, "b": 2, "c__a": 3, "c__b": 4},
    {"a": 1, "b": 2, "c__a": 5, "c__b": 6},
    {"a": 1, "b": 2, "c__a": 7, "c__b": 8},
]
_expected2 = {"a": 1, "b": 2, "c": [{"a": 3, "b": 4}, {"a": 5, "b": 6}, {"a": 7, "b": 8}]}
_expected2_cb = {"a": 1, "b": 2, "c": [{"a": 3}, {"a": 5}, {"a": 7}]}
_expected2_cb2 = {"a": 1, "b": 2, "c": [{"a": 3, "b": 44}, {"a": 5, "b": 66}, {"a": 7, "b": 88}]}


def test_auto_group_list():
    assert auto_group_list(_input2) == _expected2


def test_auto_group_list_callback():
    assert auto_group_list(_input2, each_result_row_callback=lambda item: item.pop("b")) == _expected2_cb


def test_auto_group_list_callback2():
    def my_callback(item):
        item["b"] = item["b"] * 11

    assert auto_group_list(_input2, each_result_row_callback=my_callback) == _expected2_cb2


# #### test 3
_input3 = {"a": 1, "b": 2, "c___a": 3, "c___b___bb": 4, "c___b___cc": 5}
_expected3 = {"a": 1, "b": 2, "c": {"a": 3, "b": {"bb": 4, "cc": 5}}}

auto_group_dict_input = [(_input3, _expected3)]


def test_auto_group_dict():
    assert auto_group_dict(_input3) == _expected3


# #### test 4
_input4 = [
    {"a": 1, "b": 2, "c__a": 3, "c__b": 4},
    {"a": 1, "b": 2, "c__a": 5, "c__b": 6},
    {"a": 1, "b": 2, "c__a": 7, "c__b": 8},
    {"a": 11, "b": 22, "c__a": 44, "c__b": 55},
    {"a": 11, "b": 22, "c__a": 66, "c__b": 77},
    {"a": 11, "b": 22, "c__a": 88, "c__b": 99},
]
_expected4 = [
    {"a": 1, "b": 2, "c": [{"a": 3, "b": 4}, {"a": 5, "b": 6}, {"a": 7, "b": 8}]},
    {"a": 11, "b": 22, "c": [{"a": 44, "b": 55}, {"a": 66, "b": 77}, {"a": 88, "b": 99}]}
]

_expected4_cb = [
    {"a": 1, "c": [{"a": 3, "b": 4}, {"a": 5, "b": 6}, {"a": 7, "b": 8}]},
    {"a": 11, "c": [{"a": 44, "b": 55}, {"a": 66, "b": 77}, {"a": 88, "b": 99}]}
]


def test_auto_group_list_multi():
    assert auto_group_list_multi(_input4) == _expected4


def test_auto_group_list_multi_callback():
    assert auto_group_list_multi(_input4, each_result_row_callback=lambda item: item.pop("b")) == _expected4_cb


def test_auto_group_list_multi_callback2():
    assert auto_group_list_multi(_input4, each_result_row_callback=lambda item: dict_filter(item, ["b"])) == _expected4_cb
