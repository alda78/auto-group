from auto_group.tools import sort_list_of_dicts
from auto_group.tools import dict_filter
from auto_group.tools import dict_pass
from auto_group.tools import dict_swap
from auto_group.tools import map_dict_into_list
from auto_group.tools import chunk_list


example1 = [
    {"id": 4, "name": "AAA"},
    {"id": 3, "name": "BBB"},
    {"id": 2, "name": "CCC"},
    {"id": 1, "name": "DDD"},
]
example2 = [
    {"id": 1, "name": "DDD"},
    {"id": 2, "name": "CCC"},
    {"id": 3, "name": "BBB"},
    {"id": 4, "name": "AAA"},
]
dict_example = {"a": 1, "b": 2, "c": 3, "d": 4}


def test_sort_list_of_dicts():
    assert sort_list_of_dicts(example1, "id+") == example2
    assert sort_list_of_dicts(example1, "name+") == example1
    assert sort_list_of_dicts(example1, "id-") == example1
    assert sort_list_of_dicts(example1, "name-") == example2


def test_dict_filter():
    assert dict_filter(dict_example, ["a", "b"]) == {"c": 3, "d": 4}


def test_dict_pass():
    assert dict_pass(dict_example, ["a", "b"]) == {"a": 1, "b": 2}


def test_dict_swap():
    l = ["a", "b", "c"]
    d = dict(enumerate(l))
    ds = dict_swap(d)
    assert d == {0: "a", 1: "b", 2: "c"}
    assert ds == {"a": 0, "b": 1, "c": 2}


def test_map_dict_into_list():
    map = ["c", "a", "b", "d"]
    d = {"a": 1, "b": 2, "c": 3, "d": 4}
    assert map_dict_into_list(d, map) == [3, 1, 2, 4]


def test_map_dict_into_list_2():
    map = ["c", "a", "b", "d"]
    d = {"a": "A value", "b": "B value", "c": "C value", "d": "D value"}
    assert map_dict_into_list(d, map) == ["C value", "A value", "B value", "D value"]


def test_chunk_list():
    l = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    for chunk_number, chunk in enumerate(chunk_list(l, 3)):
        if chunk_number == 0:
            assert chunk == [1, 2, 3]
        if chunk_number == 1:
            assert chunk == [4, 5, 6]
        if chunk_number == 2:
            assert chunk == [7, 8, 9]
        if chunk_number == 3:
            assert chunk == [10]

    assert list(chunk_list(l, 3)) == [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10]]
